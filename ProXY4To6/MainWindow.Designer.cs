﻿namespace ProXY4To6
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxListenPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxProxyIP = new System.Windows.Forms.TextBox();
            this.textBoxProxyPort = new System.Windows.Forms.TextBox();
            this.comboBoxProxy = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonHostIP = new System.Windows.Forms.Button();
            this.buttonProxyUpdate = new System.Windows.Forms.Button();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuNotify = new System.Windows.Forms.ContextMenu();
            this.menuItemNotifyStop = new System.Windows.Forms.MenuItem();
            this.menuItemNotifyBoot = new System.Windows.Forms.MenuItem();
            this.menuItemNotifyQuit = new System.Windows.Forms.MenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxProxyState = new System.Windows.Forms.TextBox();
            this.labelState = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(2, 237);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(401, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "新版由<大约在秋季>维护                       旧版由<落叶清风>编写 ";
            // 
            // textBoxListenPort
            // 
            this.textBoxListenPort.BackColor = System.Drawing.Color.GhostWhite;
            this.textBoxListenPort.Location = new System.Drawing.Point(337, 132);
            this.textBoxListenPort.Name = "textBoxListenPort";
            this.textBoxListenPort.Size = new System.Drawing.Size(66, 21);
            this.textBoxListenPort.TabIndex = 2;
            this.textBoxListenPort.Text = "2012";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "域名：";
            // 
            // buttonOK
            // 
            this.buttonOK.BackColor = System.Drawing.Color.Black;
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonOK.ForeColor = System.Drawing.Color.White;
            this.buttonOK.Location = new System.Drawing.Point(305, 162);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(98, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "开始代理";
            this.buttonOK.UseVisualStyleBackColor = false;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Location = new System.Drawing.Point(2, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "地址：";
            // 
            // textBoxProxyIP
            // 
            this.textBoxProxyIP.Location = new System.Drawing.Point(41, 44);
            this.textBoxProxyIP.Name = "textBoxProxyIP";
            this.textBoxProxyIP.Size = new System.Drawing.Size(209, 21);
            this.textBoxProxyIP.TabIndex = 6;
            // 
            // textBoxProxyPort
            // 
            this.textBoxProxyPort.Location = new System.Drawing.Point(256, 44);
            this.textBoxProxyPort.Name = "textBoxProxyPort";
            this.textBoxProxyPort.Size = new System.Drawing.Size(41, 21);
            this.textBoxProxyPort.TabIndex = 7;
            // 
            // comboBoxProxy
            // 
            this.comboBoxProxy.FormattingEnabled = true;
            this.comboBoxProxy.Location = new System.Drawing.Point(41, 13);
            this.comboBoxProxy.Name = "comboBoxProxy";
            this.comboBoxProxy.Size = new System.Drawing.Size(209, 20);
            this.comboBoxProxy.TabIndex = 8;
            this.comboBoxProxy.SelectedIndexChanged += new System.EventHandler(this.comboBoxProxy_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(297, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "监听:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(256, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "端口：";
            // 
            // buttonHostIP
            // 
            this.buttonHostIP.Location = new System.Drawing.Point(306, 13);
            this.buttonHostIP.Name = "buttonHostIP";
            this.buttonHostIP.Size = new System.Drawing.Size(98, 20);
            this.buttonHostIP.TabIndex = 11;
            this.buttonHostIP.Text = "域名解释";
            this.buttonHostIP.UseVisualStyleBackColor = true;
            this.buttonHostIP.Click += new System.EventHandler(this.buttonHostIP_Click);
            // 
            // buttonProxyUpdate
            // 
            this.buttonProxyUpdate.Location = new System.Drawing.Point(305, 44);
            this.buttonProxyUpdate.Name = "buttonProxyUpdate";
            this.buttonProxyUpdate.Size = new System.Drawing.Size(98, 20);
            this.buttonProxyUpdate.TabIndex = 12;
            this.buttonProxyUpdate.Text = "代理更新";
            this.buttonProxyUpdate.UseVisualStyleBackColor = true;
            this.buttonProxyUpdate.Click += new System.EventHandler(this.buttonProxyUpdate_Click);
            // 
            // buttonAbout
            // 
            this.buttonAbout.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAbout.Location = new System.Drawing.Point(306, 211);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(97, 23);
            this.buttonAbout.TabIndex = 13;
            this.buttonAbout.Text = "关于更新";
            this.buttonAbout.UseVisualStyleBackColor = true;
            this.buttonAbout.Click += new System.EventHandler(this.buttonAbout_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenu = this.contextMenuNotify;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "ProXY4To6";
            this.notifyIcon.Visible = true;
            // 
            // contextMenuNotify
            // 
            this.contextMenuNotify.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemNotifyStop,
            this.menuItemNotifyBoot,
            this.menuItemNotifyQuit});
            // 
            // menuItemNotifyStop
            // 
            this.menuItemNotifyStop.Index = 0;
            this.menuItemNotifyStop.Text = "停止";
            this.menuItemNotifyStop.Click += new System.EventHandler(this.menuItemNotifyStop_Click);
            // 
            // menuItemNotifyBoot
            // 
            this.menuItemNotifyBoot.Index = 1;
            this.menuItemNotifyBoot.Text = "启动";
            this.menuItemNotifyBoot.Click += new System.EventHandler(this.menuItemNotifyBoot_Click);
            // 
            // menuItemNotifyQuit
            // 
            this.menuItemNotifyQuit.Index = 2;
            this.menuItemNotifyQuit.Text = "退出";
            this.menuItemNotifyQuit.Click += new System.EventHandler(this.menuItemNotifyQuit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = global::ProXY4To6.Properties.Resources.cl800x600_1;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-3, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(409, 254);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxProxyState
            // 
            this.textBoxProxyState.Location = new System.Drawing.Point(337, 105);
            this.textBoxProxyState.Name = "textBoxProxyState";
            this.textBoxProxyState.Size = new System.Drawing.Size(66, 21);
            this.textBoxProxyState.TabIndex = 14;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.labelState.Location = new System.Drawing.Point(297, 108);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(35, 12);
            this.labelState.TabIndex = 15;
            this.labelState.Text = "速度:";
            // 
            // MainWindow
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 258);
            this.Controls.Add(this.labelState);
            this.Controls.Add(this.textBoxProxyState);
            this.Controls.Add(this.buttonAbout);
            this.Controls.Add(this.buttonProxyUpdate);
            this.Controls.Add(this.buttonHostIP);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxProxy);
            this.Controls.Add(this.textBoxProxyPort);
            this.Controls.Add(this.textBoxProxyIP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxListenPort);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxListenPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxProxyIP;
        private System.Windows.Forms.TextBox textBoxProxyPort;
        private System.Windows.Forms.ComboBox comboBoxProxy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonHostIP;
        private System.Windows.Forms.Button buttonProxyUpdate;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.MenuItem menuItemNotifyStop;
        private System.Windows.Forms.ContextMenu contextMenuNotify;
        private System.Windows.Forms.MenuItem menuItemNotifyBoot;
        private System.Windows.Forms.MenuItem menuItemNotifyQuit;
        private System.Windows.Forms.TextBox textBoxProxyState;
        private System.Windows.Forms.Label labelState;
    }
}