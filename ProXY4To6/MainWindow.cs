﻿/*
    Copyright ?2011, zghaia@gmail.com
    All rights reserved.
    http://code.google.com/p/zghaia-ipv6proxy-proxy4to6/

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    - Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer. 

    - Neither the name of the zghaia@gmail.com, nor the names of its contributors
       may be used to endorse or promote products derived from this
       software without specific prior written permission. 

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Org.Mentalis.Proxy.Http;

namespace ProXY4To6
{
    public partial class MainWindow : Form
    {
        private bool BUpdate = true;
        private bool BNotify = false;
        private string StrVersion =AboutBox.AssemblyProduct+" v"+AboutBox.AssemblyVersion;
        public MainWindow()
        {
            InitializeComponent();
            this.Text = AboutBox.AssemblyTitle;
            this.comboBoxProxy.Text = ProxyManeger.getProxyDefault("domain");
            this.textBoxListenPort.Text = ProxyManeger.getProxyDefault("listen");
            if(this.comboBoxProxy.Text=="")
            {
                this.textBoxProxyIP.Text = ""; 
                this.textBoxProxyPort.Text ="";
            }
            else
            {
                this.textBoxProxyIP.Text = ProxyManeger.getProxyIP(comboBoxProxy.Text); 
                this.textBoxProxyPort.Text = ProxyManeger.getProxyPort(comboBoxProxy.Text); 
            }
            this.SizeChanged += new System.EventHandler(this.MainWindow_SizeChanged);
            this.comboBoxProxy.Click += new EventHandler(comboBoxProxy_Click);
            this.comboBoxProxy.SelectedIndexChanged += new EventHandler(comboBoxProxy_SelectedIndexChanged);
            this.notifyIcon.DoubleClick += new EventHandler(notifyIcon_DoubleClick);
            this.menuItemNotifyStop.Click += new System.EventHandler(this.buttonOK_Click);
            this.menuItemNotifyBoot.Click += new System.EventHandler(this.buttonOK_Click);
            this.menuItemNotifyQuit.Click += new System.EventHandler(this.buttonOK_Click);
        }
      
        HttpListener lis;
        private HttpListener Lis
        {
            get
            { 
                return lis; 
            }
            set 
            {
                if (lis != null)
                {
                    lis.Dispose();
                }
                lis = value;

            }
        }
     
        Timer m_timer;
        private Timer MainTimer
        {
            get 
            {
                if (m_timer == null)
                {
                    m_timer = new Timer();
                }
                return m_timer;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            
            if (buttonOK.Text == "开始代理")
            {
                if ((textBoxProxyIP.Text != "") && (textBoxProxyPort.Text != "") && (textBoxListenPort.Text != ""))
                {
                    try  
                    {
                        lis = new HttpListener(Convert.ToInt32(textBoxListenPort.Text));
                        textBoxProxyState.Text=lis.getProxyState((string)textBoxProxyIP.Text).ToString()+"ms";
                        if(textBoxProxyState.Text=="0ms")
                        {
                            ProxyStop();
                            MessageBox.Show("代理连接速度为零，\n最好换用别的代理");
                        }
                        else
                        {
                            lis.SetProxyParam((string)textBoxProxyIP.Text, Convert.ToInt32(textBoxProxyPort.Text));
                            lis.Start();
                            ProxyManeger.updateProxyDefault((string)comboBoxProxy.Text, (string)textBoxListenPort.Text);
                            buttonOK.Text = "停止代理";
                            textBoxListenPort.Enabled = false;
                            MainTimer.Interval = 1000;
                            MainTimer.Tick += new EventHandler(MainTimer_Tick);
                            MainTimer.Start();
                        }
                   }
                   catch (Exception ex)
                   {
                        MessageBox.Show("端口可能占用\n参照主页wiki\n" + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("输入不能有空");
                }
            }
            else
            {
                ProxyStop();
            }
        }

        void ProxyStop()
        {
            buttonOK.Text = "开始代理";
            textBoxListenPort.Enabled = true;
            BNotify = false;
            this.Text = StrVersion + " BY 大约在秋季";
            MainTimer.Stop();
            if (lis != null)
            {
                lis.Dispose();
            }
        }

        void MainTimer_Tick(object sender, EventArgs e)
        {
            int count = lis.GetClientCount();
            this.Text = StrVersion + "  <" + count + ">" + "连接";
            if (count > 0)
            {
                BNotify = true;
            }
            if (BNotify && (count == 0))
            {
                if (lis.getProxyState((string)textBoxProxyIP.Text) == 0)
                {
                     BNotify = false;
                     notifyIcon.ShowBalloonTip
                     (
                         3000, 
                         "ProXY4To6 提示",
                         "代理服务器连接速度为零，\n可能代理服务器失去连接，\n可换用别的服务器继续使用",
                         notifyIcon.BalloonTipIcon
                     );
                }
            }
        }

        private void comboBoxProxy_Click(object sender, EventArgs e)
        {
            if (BUpdate)
            {
                comboBoxProxy.Items.Clear();
                ArrayList Proxy = ProxyManeger.getProxyDomains();
                comboBoxProxy.BeginUpdate();
                for (int i = 0; i < Proxy.Count; i++)
                {
                    comboBoxProxy.Items.Add(Proxy[i]);
                }
                comboBoxProxy.EndUpdate();
                BUpdate = false;
            }
        }

        private void comboBoxProxy_SelectedIndexChanged(object sender, EventArgs e)
        {
         
            ComboBox comboBox = (ComboBox)sender;
            string  selectedDomain= (string)comboBoxProxy.SelectedItem;
            if (selectedDomain != "")
            {
                int resultIndex = -1;

                resultIndex = comboBoxProxy.FindStringExact(selectedDomain);
                textBoxProxyIP.Text = ProxyManeger.getProxyIP(selectedDomain);
                textBoxProxyPort.Text = ProxyManeger.getProxyPort(selectedDomain);
            }
            else
            {
                textBoxProxyIP.Text = "";
                textBoxProxyPort.Text = "";
            }

        }
        
        private void buttonHostIP_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxProxy.Text != "")
                {
                    textBoxProxyIP.Text = ("[" + ProxyManeger.getDomainIP(comboBoxProxy.Text) + "]");
                    Text = StrVersion + "    域名解释成功";
                }
                else
                {
                    MessageBox.Show("域名未被选择");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("域名解释失败" + ex.Message);
            }
        }

        private void buttonProxyUpdate_Click(object sender, EventArgs e)
        {
            this.Text =StrVersion + " 代理正在更新";
            string StrProxyDomain = comboBoxProxy.Text;
            string StrProxyIP = textBoxProxyIP.Text;
            string StrProxyPort = textBoxProxyPort.Text;
            if ((StrProxyDomain != "") && (StrProxyIP != "") && (StrProxyPort != ""))
            {
                ProxyManeger.updateProxyList(StrProxyDomain, StrProxyIP, StrProxyPort);
                Text = StrVersion+" 代理更新完毕";
                BUpdate = true;
            }
            else
            {
                MessageBox.Show("输入不能为空");
            }
        }

        private  void buttonAbout_Click(object sender, EventArgs e)
        {
            AboutBox FormAbout= new AboutBox();
            FormAbout.Show(); 
        }

        private void MainWindow_SizeChanged(object sender, EventArgs e)
        {

            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
            }

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void menuItemNotifyStop_Click(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip(3000, "ProXY4To6 提示", "代理已经停止运行", notifyIcon.BalloonTipIcon);
        }

        private void menuItemNotifyBoot_Click(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip(3000, "ProXY4To6 提示", "代理已经开始运行", notifyIcon.BalloonTipIcon);
        }

        private void menuItemNotifyQuit_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Hide();
            }
            else
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

    }
}
