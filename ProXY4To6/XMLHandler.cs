﻿/*
    Copyright ?2011, zghaia@gmail.com
    All rights reserved.
    http://code.google.com/p/zghaia-ipv6proxy-proxy4to6/

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    - Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer. 

    - Neither the name of the zghaia@gmail.com, nor the names of its contributors
       may be used to endorse or promote products derived from this
       software without specific prior written permission. 

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Text;
using System.Collections;
using System.Xml;

namespace ProXY4To6
{
    class XMLHandler
    {

        public static ArrayList getXMLKeys(string tag, string key, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            ArrayList StrKey = new ArrayList();
            XmlNodeList nodes = doc.GetElementsByTagName(tag);
            for (int i = 0; i < nodes.Count; i++)
            {
                XmlAttribute att = nodes[i].Attributes[key];
                StrKey.Add(att.Value);
            }
            return StrKey;
        }

        public static void creatXMLtag(string rootTag, string downTag, string[][] arrayTag, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNode root = doc.SelectSingleNode(rootTag);  
            XmlElement down = doc.CreateElement(downTag);  
            for (int i = 0; i < arrayTag.Length; i++)
            {
                down.SetAttribute(arrayTag[i][0], arrayTag[i][1]);
            }
            root.AppendChild(down);
            doc.Save(path);
        }

        public static void changeXMLtag(string tag,int index,string[][] arrayTag, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNodeList nodes = doc.GetElementsByTagName(tag);
            for (int i = 0; i < arrayTag.Length; i++)
            {
                XmlAttribute att = nodes[index].Attributes[arrayTag[i][0]];
                att.Value = arrayTag[i][1];
             }
            doc.Save(path);
        }

        public static void delXMLtag(string rootTag, string downTag, int index, string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNode root = doc.SelectSingleNode(rootTag);
            XmlNodeList nodes = doc.GetElementsByTagName(downTag);
            root.RemoveChild(nodes[index]);
            doc.Save(path);
        }
    }
}
