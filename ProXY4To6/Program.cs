﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Org.Mentalis.Proxy.Http;
namespace ProXY4To6
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());  
        }
    }
}
