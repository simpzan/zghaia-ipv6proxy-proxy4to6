﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过下列属性集
// 控制。更改这些属性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("ProXY4To6 2.4 by <zghaia@gmail.com>")]
[assembly: AssemblyDescription("新版由<大约在秋季>维护 旧版由<落叶清风>编写" + "\r\n" +"\r\n"+"                   郑重声明"+"\r\n"+"如果因为利用本软件干坏事而导致的一切后果自负"+"\r\n"+"\r\n"+"   一个用于在IPv6环境下设置全局代理的软件，主要利用了几个已知的IPv6服务器提供的免费IPv6代理.解决了在IPv6 环境下QQ等即时通信软件不能使用IPv6 代理的问题，详情请看上面的项目主页链接")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("UESTC")]
[assembly: AssemblyProduct("ProXY4To6")]
[assembly: AssemblyCopyright("Copyright © UESTC 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 属性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("fd970928-380d-4448-9d60-e4d3f6fe81c3")]

// 程序集的版本信息由下面四个值组成:
//
//      主版本
//      次版本 
//      内部版本号
//      修订号
//
// 可以指定所有这些值，也可以使用“内部版本号”和“修订号”的默认值，
// 方法是按如下所示使用“*”:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.4.0.0")]
[assembly: AssemblyFileVersion("2.4.0.0")]
