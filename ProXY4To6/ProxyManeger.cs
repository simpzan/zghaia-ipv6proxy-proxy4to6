﻿/*
    Copyright ?2011, zghaia@gmail.com
    All rights reserved.
    http://code.google.com/p/zghaia-ipv6proxy-proxy4to6/

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    - Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer. 

    - Neither the name of the zghaia@gmail.com, nor the names of its contributors
       may be used to endorse or promote products derived from this
       software without specific prior written permission. 

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using System;
using System.Text;
using System.Net;
using System.Collections;

namespace ProXY4To6
{
    class ProxyManeger
    {
        public static string getDomainIP(string domain)
        {

            try
            {
                IPAddress[] ips = new IPAddress[1];
                ips = Dns.GetHostAddresses(domain);
                return ips[0].ToString();
            }
            catch (Exception err)
            {
                Console.WriteLine("Error:{0}", err.Message);
            }
            return 0.ToString();

        }

        public static ArrayList getProxyDomains()
        {
            return XMLHandler.getXMLKeys("proxy", "domain", m_Proxyconfig);
        }

        public static ArrayList getProxyIPs()
        {
            return XMLHandler.getXMLKeys("proxy", "ip", m_Proxyconfig);
        }

        public static ArrayList getProxyPorts()
        {
            return XMLHandler.getXMLKeys("proxy", "port", m_Proxyconfig);
        }

        public static string getProxyIP(string ProxyDomain)
        {
            return (string)getProxyIPs()[getProxyDomains().IndexOf(ProxyDomain)];
        }

        public static string getProxyPort(string ProxyDomain)
        {
            return (string)getProxyPorts()[getProxyDomains().IndexOf(ProxyDomain)];
        }

        public static void updateProxyList(string ProxyDomain, string ProxyIP, string ProxyPort)
        {
            string[][] ArrayProxy = new string[3][]
            {
                    new string[] { "domain", ProxyDomain },
                    new string[] { "ip", ProxyIP }, 
                    new string[] { "port", ProxyPort }
             };
            int ProxyIndex = (int)getProxyDomains().IndexOf(ProxyDomain);
            if (ProxyIndex != -1)
            {
                if ((getProxyIP(ProxyDomain) == ProxyIP) && (getProxyPort(ProxyDomain) == ProxyPort))
                {
                    XMLHandler.delXMLtag("settings", "proxy", ProxyIndex, m_Proxyconfig);
                }
                else
                {
                    XMLHandler.changeXMLtag("proxy", ProxyIndex, ArrayProxy, m_Proxyconfig);
                }
            }
            else
            {
                XMLHandler.creatXMLtag("settings", "proxy", ArrayProxy, m_Proxyconfig);
            }
        }

        public static string getProxyDefault(string key)
        {
            return XMLHandler.getXMLKeys("default", key, m_Proxyconfig)[0].ToString();
        }

        public static void updateProxyDefault(string DefaultDomain,string DefaultListen)
        {
            string[][] DefaultProxy = new string[2][]
            {
                    new string[] { "domain", DefaultDomain },
                    new string[] { "listen", DefaultListen }, 
             };
            XMLHandler.changeXMLtag("default", 0, DefaultProxy, m_Proxyconfig);
        }

        private static string m_Proxyconfig= AppDomain.CurrentDomain.BaseDirectory.ToString()+ "ProXY4To6.config";
    }
}
